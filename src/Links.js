/**
 * Handle hash locations
 */
export default class Links {
  /**
   * Construct new object from form element
   */
  constructor(elem, history) {
    // This binding is necessary to make `this` work in the callback
    this.boundNavigate = (e) => this.navigate(e);
    this.history = history;

    /**
     * Add click listener to all links
     */
    const excludedLinks = Array.from(elem.querySelectorAll('div[data-nolinks] a'));
    const allLinks = Array.from(elem.getElementsByTagName('a'));
    this.links = allLinks.filter((htmlElement) => {
      const includes = excludedLinks.includes(htmlElement);
      return !includes;
    });
    this.length = this.links.length;
    this.addListeners();
  }

  /**
   * Loop over all links to bind scroll function
   *
   * @return void
   */
  addListeners() {
    let i = 0;

    for (i; i < this.length; i += 1) {
      const link = this.links[i];

      if (link.hasAttribute('href')) {
        const href = link.getAttribute('href');

        if (href.length > 0) {
          const isHash = href[0] === '#';

          if (isHash && href.length === 1) {
            link.addEventListener('click', Links.preventDefault, false);
          } else if (!link.hasAttribute('target') && !isHash) {
            link.addEventListener('click', this.boundNavigate, false);
          }
        }
      }
    }
  }

  /**
   * Loop over all links to remove scroll function
   *
   * @return void
   */
  removeListeners() {
    let i = 0;

    for (i; i < this.length; i += 1) {
      const link = this.links[i];

      if (link.hasAttribute('href')) {
        const href = link.getAttribute('href');

        if (href.length > 0) {
          const isHash = href[0] === '#';

          if (isHash && href.length === 1) {
            link.removeEventListener('click', Links.preventDefault, false);
          } else if (!link.hasAttribute('target') && !isHash) {
            link.removeEventListener('click', this.boundNavigate, false);
          }
        }
      }
    }
  }

  /**
   * Navigate to new page
   *
   * @param e object Mouse event that's triggered after link click
   *
   * @return void
   */
  navigate(e) {
    e.preventDefault();

    // Get href from elem
    const href = e.currentTarget.getAttribute('href');
    const target = e.currentTarget.getAttribute('target');

    // URL checks
    const hasProtocol = href.includes('http://') || href.includes('https://');
    const isOrigin = href.includes(window.location.origin);
    const isExternal = hasProtocol && !isOrigin;
    const isSelf = hasProtocol && target === '_self';

    // Pathname checks
    const pathname = hasProtocol ? new URL(href).pathname : href;
    const isDocument = pathname.split('/').pop().indexOf('.') > 0;
    const isMail = href.includes('mailto:');
    const isTel = href.includes('tel:');

    if (target === '_blank') {
      window.open(href, '_blank');
    } else if (isExternal || isDocument || isMail || isTel || isSelf) {
      window.location.href = href;
    } else {
      this.history.push(href.replace(window.location.origin, ''));
    }
  }

  /**
   * Prevent normal behaviour
   *
   * @param e object Mouse event that's triggered after link click
   *
   * @return void
   */
  static preventDefault(e) {
    e.preventDefault();
  }
}
